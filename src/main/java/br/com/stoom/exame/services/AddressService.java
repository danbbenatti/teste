package br.com.stoom.exame.services;



import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.stoom.exame.domain.Address;
import br.com.stoom.exame.repositories.AddressRepository;
import br.com.stoom.exame.util.GetAddressByAPI;
import javassist.tools.rmi.ObjectNotFoundException;

@Service
public class AddressService {
	
	@Autowired
	AddressRepository repo;
	
	public List<Address> getAddressByZipcode(String zipcode){
		List<Address> address = repo.findAddressByZipcode(zipcode);
		
		return address;
		
	}
	
	public Address searchAddressId(Integer id) throws ObjectNotFoundException {
		Optional<Address> address = repo.findById(id);
		if(address ==null) {
			throw new ObjectNotFoundException("Address was not found with this ID: " + id +", Type: " +Address.class.getName());
		}
		return address.orElse(null);
	}
	
	public Address insertAddress(Address address) {
		address.setId(null);
			if(address.getLatitude()==null || address.getLongitude()==null ) {
				address = GetAddressByAPI.getLatLng(address);
			}
		
		
		return repo.save(address);
	}
	
	public List<Address> insertAddress(List<Address> address) {	
		for(Address a : address) {
			if(a.getLatitude()==null || a.getLongitude()==null ) {
				a = GetAddressByAPI.getLatLng(a);
			}
		}
		
		return repo.saveAll(address);
	}

	public Address updateAddress(Address address) throws ObjectNotFoundException {	
		searchAddressId(address.getId());
		if(address.getLatitude()==null || address.getLongitude()==null ) {
			address = GetAddressByAPI.getLatLng(address);
		}	
		return repo.save(address);
	}

	public void deleteAddress(Integer id) throws ObjectNotFoundException {
		searchAddressId(id);
		repo.deleteById(id);
		// TODO Auto-generated method stub
		
	}
}
