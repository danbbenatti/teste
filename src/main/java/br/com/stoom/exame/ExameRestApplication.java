package br.com.stoom.exame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExameRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExameRestApplication.class, args);
	}

}
